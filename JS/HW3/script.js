// 1. Порожній прямокутник

for (let a = 0; a < 10; ++a) {
  if (a == 0 || a == 9)
    for (let i = 0; i <= 9; i++) {
      document.write("*");
    }
  else
    for (let i = 0; i <= 9; i++)
      if (i == 0 || i == 9) {
        document.write("*");
      } else {
        document.write("&nbsp;&nbsp;");
      }
  document.write("</br>");
}

// 2. Заповнений рівносторонній трикутник

for (let i = 0; i < 10; ++i) {
  for (let spase = 10 - i; spase > 0; spase--) {
    document.write("&nbsp;&nbsp;");
  }
  for (let j = 0; j <= i; j++)
    if (j == 0) {
      document.write("*");
    } else {
      document.write("**");
    }
  document.write("<br>");
}

// 3. Заповнений прямокутний трикутник

for (let i = 0; i < 10; ++i) {
  for (let j = 0; j <= i; j++){
      document.write("*");
    }
  document.write("<br>");
}

// 4. Заповнений ромб

for (let i = 0; i < 21; ++i) {
  if (i < 10) {
    for (let spase = 10 - i; spase > 0; spase--) {
      document.write("&nbsp;&nbsp;");
    }
    for (let j = 0; j <= i; j++)
      if (j == 0) {
        document.write("*");
      } else {
        document.write("**");
      }
    }
  else if (i>10) {
      for (let spase = 10 - i; spase < 0; spase++) {
        document.write("&nbsp;&nbsp;");
      }
      for (let j = i; j <21; j++)
        if (j ==20) {
          document.write("*");
        } else {
          document.write("**");
          }
    }
  else {
      for (let j = 0; j <= 20; ++j){
          document.write("*");
      }
    }
      
  document.write("<br>");
}


// 5. Створіть масив styles з елементами «Джаз» та «Блюз». 
// Додайте «Рок-н-рол» до кінця.
// Замініть значення всередині на «Класика». 
// Ваш код для пошуку значення всередині повинен працювати для масивів з будь - якою довжиною
// Видаліть перший елемент масиву та покажіть його.
// Вставте «Реп» та «Реггі» на початок масиву.

const STYLES = ['Джаз', 'Блюз'];
STYLES.push("Рок-н-рол");
STYLES.splice(1, 1, "Класика");
const a = STYLES.shift();
console.log(a);
STYLES.unshift("Реп", "Реггі");
console.log(S);

